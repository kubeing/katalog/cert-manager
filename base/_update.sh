#!/bin/bash
# If the first argument is set, use that ss Version, otherwise use the latest version starting with v
# Might run on a mac, so use gnu sed etc
set -ex
VERSION=${1:-$(curl -s https://api.github.com/repos/cert-manager/cert-manager/releases | grep tag_name | grep v | head -n 1 | cut -d '"' -f 4)}
echo $VERSION
curl -L https://github.com/cert-manager/cert-manager/releases/download/${VERSION}/cert-manager.yaml > $(dirname $0)/cert-manager.yaml
